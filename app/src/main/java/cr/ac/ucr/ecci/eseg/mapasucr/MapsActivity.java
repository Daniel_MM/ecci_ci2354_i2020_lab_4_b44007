package cr.ac.ucr.ecci.eseg.mapasucr;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMapLongClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener  {

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private static final int LOCATION_REQUEST_CODE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    // Implementamos las llamadas al menú de opciones
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_1) {
            marcadorSodaUCR();
            return true;
        } else if (id == R.id.action_2) {
            mostrarZoom();
            return true;
        } else if (id == R.id.action_3) {
            posicionCamaraZoom();
            return true;
        } else if (id == R.id.action_4) {
            MapaSatelite();
            return true;
        } else if (id == R.id.action_5) {
            MapaHibrido();
            return true;
        } else if (id == R.id.action_6) {
            MapaNinguno();
            return true;
        } else if (id == R.id.action_7) {
            MapaNormal();
            return true;
        } else if (id == R.id.action_8) {
            MapaTierra();
            return true;
        }else if(id == R.id.action_9){
            obtenerLocalizacion();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            solicitarPermiso(Manifest.permission.ACCESS_FINE_LOCATION, LOCATION_REQUEST_CODE);
        }
        mMap.setOnMapClickListener(this);
        mMap.setOnMapLongClickListener(this);
    }

    protected void solicitarPermiso(String permissionType, int requestCode) {
        ActivityCompat.requestPermissions (this, new String []{permissionType}, requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
            }
        }
    }

    public void marcadorSodaUCR() {
        if (mMap != null) {
            LatLng sodaOdontologia = new LatLng(9.938035, -84.051509);
            mMap.addMarker(new MarkerOptions().position(sodaOdontologia).title("Soda de Odontología de la UCR"));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(sodaOdontologia));
        }
    }

    // Mostrar las opciones de Zoom
    public void mostrarZoom() {
        UiSettings mapSettings;
        mapSettings = mMap.getUiSettings();
        mapSettings.setZoomControlsEnabled(true);
        mapSettings.setCompassEnabled(true);
        mapSettings.setMyLocationButtonEnabled(true);
    }

    // Mover la posicion de la camara y hacer Zoom
    public void posicionCamaraZoom() {
        LatLng sodaOdontologia = new LatLng(9.938035, -84.051509);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(sodaOdontologia)
                .zoom(15) // zoom level
                .bearing(70) // bearing // direccion de la camara
                .tilt(25) // tilt angle // inclinacion
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void MapaSatelite() {
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
    }

    public void MapaHibrido() {
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }

    public void MapaNinguno() {
        mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
    }

    public void MapaNormal() {
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }

    public void MapaTierra() {
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        Toast.makeText(getApplicationContext(), latLng.toString(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(latLng.toString())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        Toast.makeText(getApplicationContext(),
                "Nuevo marcador: " + latLng.toString(), Toast.LENGTH_LONG).show();
    }

    private void obtenerLocalizacion() {
        if (mMap == null) {
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location miLocalizacion = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(miLocalizacion.getLatitude(), miLocalizacion.getLongitude()))
                .zoom(15) // zoom level
                .bearing(70) // bearing // direccion de la camara
                .tilt(25) // tilt angle // inclinacion
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }
    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

}